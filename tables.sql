CREATE TABLE "Interests" (
  "Id" int,
  "Name" varchar(100),
  PRIMARY KEY ("Id")
);

CREATE TABLE "Status" (
  "Id" int,
  "Name" varchar(100),
  PRIMARY KEY ("Id")
);

CREATE TABLE "Participant" (
  "Email" varchar(100),
  "Name" varchar(100),
  "LastName" varchar(100),
  "KaggleId" varchar(100),
  "GitlabUsername" varchar(100),
  "DateOfBirth" date,
  "Phone" varchar(10),
  "Description" varchar(1000),
  "Status" int,
  PRIMARY KEY ("Email"),
  CONSTRAINT "FK_Participant.Status"
    FOREIGN KEY ("Status")
      REFERENCES "Status"("Id")
);

CREATE TABLE "ParticipantInterests" (
  "ParticipantId" varchar(100),
  "InterestId" int,
  PRIMARY KEY ("ParticipantId", "InterestId"),
  CONSTRAINT "FK_ParticipantInterests.InterestId"
    FOREIGN KEY ("InterestId")
      REFERENCES "Interests"("Id"),
  CONSTRAINT "FK_ParticipantInterests.ParticipantId"
    FOREIGN KEY ("ParticipantId")
      REFERENCES "Participant"("Email")
);
