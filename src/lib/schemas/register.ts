import { z } from 'zod';

export const registerSchema = z.object({
	name: z.string().min(1, { message: 'Name is required' }),
	lastName: z.string().max(100).min(1, { message: 'Last name is required' }),
	email: z.string().email({ message: 'Invalid email address' }),
	kaggle: z.string().max(100).min(1, { message: 'Kaggle id is required' }),
	gitlab: z.string().max(100).min(1, { message: 'Gitlab id is required' }),
	status: z.string().min(1, { message: 'Status is required' }),
	about: z.string().max(1000).optional(),
	dob: z
		.string()
		.refine((value) => z.coerce.date().max(new Date()).safeParse(value).success, {
			message: 'You are too young.'
		})
		.optional(),
	phone: z
		.undefined()
		.optional()
		.or(
			z.string().refine(
				(value) => {
					if (value === null || value === undefined) {
						return true;
					}
					if (value === '') {
						return true;
					}
					if (isNaN(Number(value))) {
						return false;
					}
					const valueAsString = value.toString();

					return valueAsString.length === 10;
				},
				{
					message: 'Phone must be 10 digits.'
				}
			)
		)
		.optional(),
	ai: z.boolean(),
	ml: z.boolean(),
	frontEnd: z.boolean(),
	backEnd: z.boolean(),
	fullStack: z.boolean()
});
